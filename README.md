# GNOME Handbook

This is an experimental handbook for GNOME contributors. The idea is that this docs project would replace the wiki as the main source of information for GNOME contributors, including information about teams, communications channels, infrastructure, processes.

Merge requests for missing or incorrect information would be encouraged.

## How to build and edit

Sphinx can be used to build and preview the static html site locally, either with the browser or VS Code.

### 1. Install dependencies

On Fedora, run:

```
dnf install -y python3-sphinx python3-pip
pip3 install --upgrade furo
```

### 2. Make changes

VS Code is a good choice for this, as it is able to preview the source files as rendered HTML.

### 3. Build

Building the docs checks for errors, as well as producing local static HTML of the HIG website.

To build, run `00localbuild.sh` from the project root. The build output can then be found in ``/build``.

### 4. Deploy changes

Changes to `main` branch are automatically deployed to the site using CI.

