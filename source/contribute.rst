How to Contribute
=================

General Guidelines
------------------

* Become an avid user
* Join the Community
* Familiarize yourself with our design
* Take some time to learn the tools
* Understand the License
* Ensure the work is your own
* Start small
* Test your changes
* Be patient 

Ways to Contribute
------------------

* Help with translations
* Help with documentation
* Help report bugs
* Help triage bugs
* Help test and assure quality
* Help with outreach and engagement
* Fix a bug
* Help out with design
* Help make an existing Application better
* Write a new Application
* Work on core OS infrastructure
* Help our system administration team
* Volunteer to help at a conference
* Become a Friend of GNOME 
