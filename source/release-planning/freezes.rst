Freezes
=======

The GNOME schedule contains dates for the following freezes. These freezes help GNOME contributors to coordinate their work, to increase quality.

Do remember, if you miss a freeze, it's not the end of the world, because the next 6-month release phase will usually only be 2 or 3 months away. Just make sure it's in a merge request on GitLab. See Approving/Rejecting freeze breaks, if you think that you need to break a freeze.

The Freeze
----------

The Freeze consists of three freezes and the string change announcement period that were previously independent:

API/ABI Freeze
--------------

No API or ABI changes should be made in the platform libraries. For instance, no new functions, no changed function signatures or struct fields.

This provides a stable development platform for the rest of the schedule. API freeze is not required for non-platform libraries, but is recommended.

To break API/ABI Freeze you need two approvals from the release team.

Feature Freeze
--------------

No new features can be added anymore so developers focus on stability and the GNOME Documentation Project can document the existing functionality. Bugfixes of existing features are still allowed.

To break Feature Freeze you need two approvals from the release team.

UI Freeze
---------

No major UI changes may be made at all without two approvals from the release team. Small fixes do not require permission.

String Change Announcement Period
---------------------------------

The time before string freeze designated as a change announcement period where string changes can still be made (e.g. for typo fixes or translatability improvements). All string changes must be announced to Discourse with the i18n tag.

String Freeze
-------------

No string changes may be made without two approvals from the Translation Coordination Team. Please open an issue using the appropriate template.

From this point, developers should concentrate on stability and bug-fixing. Translators can work without worrying that the original English strings will change, and documentation writers can take accurate screenshots.

For the string freezes explained, and to see which kind of changes are not covered by freeze rules, check TranslationProject/HandlingStringFreezes.

Hard Code Freeze
----------------

This is a late freeze to avoids sudden last-minute accidents which could risk the stability that should have been reached at this point. No source code changes are allowed without two approvals from the release team, but translation and documentation should continue. Simple build fixes are, of course, allowed without asking.

Approving/Rejecting Freeze Breaks
---------------------------------

See ReleasePlanning/RequestingFreezeBreaks
