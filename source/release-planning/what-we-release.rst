What We Release
===============

Premise
-------

The GNOME project is primarily united around a single effort -- the creation of the GNOME desktop experience.

This effort focuses on a tightly-integrated desktop environment based on the GNOME Shell running on a GNU-based operating system with a Linux kernel. Above all else, our interest is to create a cohesive product. Uses of our technologies in other environments are welcome, but are considered secondary concerns. This document attempts to clearly outline what "secondary" means. This is done in order to avoid misunderstandings and misexpectations among any parties involved or interested in interacting with and participating in the GNOME project.

This document attempts to provide an observation of the current state of things, from the perspective of the release team.

Parts of GNOME
--------------

GNOME
~~~~~

GNOME itself consists of many modules that sit on top of GLib and GTK. We are interested in creating a desktop environment as a cohesive product. By default, we do not pay too much interest to the needs of those who wish to use the environment in ways that are not part of this vision. It is not our policy to block these uses, but if there is a technical advantage to GNOME that has a disadvantage to usability of components outside of GNOME, we will favour the technical advantage.

GNOME is not intended to be portable to macOS and Windows systems. GNOME applications are not intended to be fully functional when used outside of environments based on GNOME Shell.

GNOME is a community of maintainers with high degrees of autonomy. Individual maintainers may make extra efforts to allow their software to function in alternate desktop environments or on other operating systems. It is important to note that improving functionality on non-GNOME systems often comes at the cost of increased maintainership burden. As such, this is neither encouraged nor discouraged -- it is strictly the choice of the individual maintainer. For intended integration of GNOME applications outside of supported environments it is recommended to contact maintainers first to discuss the potential extend of collaboration and related procedures.

For the purposes of this document, the gnome-build-meta core elements can serve as a definition of GNOME.

GLib/GTK
~~~~~~~~

GLib and GTK are the core parts of the GNOME platform, but are not technically parts of the GNOME project. They use GNOME infrastructure, but are not necessarily coordinated with the GNOME release cycle, nor are they subject to many GNOME policies (such as freezes). That said, GLib and GTK treat GNOME as their primary consumer and attempt to pay special attention to the needs of the GNOME project and usually follow the GNOME schedule closely.

GTK and (particularly) GLib have a wide range of deeply-invested users outside of the GNOME project. GStreamer and other GTK-based desktop environments are obvious examples. These users are welcome and highly valued. Their input on the future of GLib and GTK should be strongly considered.

Most of the people developing GLib and GTK have expertise in programming for Linux-based systems. They also tend to use these systems exclusively. GLib and GTK+ are intended to be portable to Windows and macOS, but due to the nature of those working on the two projects, support for these operating systems often lags behind. When introducing new functionalities we make efforts to at least attempt to do so in a way that considers portability to these systems, even if we don't actually provide reasonable implementations from the start. Patches and outside contributors to these areas are always welcome.

A fairly concrete definition of the GNOME development platform can be found in the gnome-build-meta sdk elements.

freedesktop.org Plumbing
~~~~~~~~~~~~~~~~~~~~~~~~

Many parts of the plumbing of a modern Linux-based operating system have been created by people who identify themselves as members of the GNOME project. In many cases, these plumbing layers are not particularly GNOME-specific, and sometimes not Linux-specific.

It is up to the individual maintainer how much they emphasizes writing code for other platforms or architectures, but publication of a project under the banner of freedesktop.org is a clear indication that this project is willing to take the needs of multiple desktop environments into account.

A fairly concrete definition of what 'plumbing' components are required for GNOME can be found in the gnome-build-meta sdk-platform elements.

Recommended components
~~~~~~~~~~~~~~~~~~~~~~

It is assumed and encouraged (but not required!) that distributions make use of the following technologies:

* Wayland
* systemd 

Concerned Parties
------------------

* XFCE, LXDE
* KDE
* MeeGo, Mer and Unity
* OpenSolaris and the BSDs
* Windows and macOS 
