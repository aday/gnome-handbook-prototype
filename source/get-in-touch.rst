Get in Touch
============

These are the primary channels that GNOME contributors use to talk to each other.

* Matrix - for instant messaging (or the legacy IRC)
* Discourse - our community forum
* GitLab - for issue tracking
    