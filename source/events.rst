Events
======

.. toctree::
   :hidden:

   events/travel


Conferences
-----------

Main regular conferences of the GNOME project are:

* GUADEC is our annual European conference, which is held in the summer.
* GNOME.ASIA Summit is an annual conference organized in Asia
* GNOME Summit is another annual event for North America. 

Hackfests
---------

TODO

Travel sponsorship
-------------------

Travel sponsorship is available through the GNOME Foundation. See :doc:`travel <events/travel>`.
