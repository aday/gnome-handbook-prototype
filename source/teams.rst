Teams
=====

There are many teams within GNOME. Here are some of the major ones:

* Accessibility - works to make GNOME accessible to anyone, irrespective of their physical abilities.
* Accounts - manages access to GNOME development systems.
* Design - GNOME user interface and UX design.
* Documentation - writes and maintains GNOME user and developer documentation.
* Engagement - GNOME websites, blogging, press, marketing & promotion activities.
* Foundation Board
* Maintainers Team - for App or Project maintainers
* Moderators - stewards of the GNOME mailing lists.
* Outreach - GNOME Outreach
* QA - GNOME QA team
* Bugsquad - helps triage bug reports and feature requests in the GNOME issue tracker. 
* Release Team - organizes and manages GNOME development.
* Safety - User safety and privacy working group.
* Sysadmin - maintain and develop GNOME's infrastructure.
* Translation - translates GNOME user interfaces and documentation into different languages.
* User Groups - Regional GNOME user groups. 
