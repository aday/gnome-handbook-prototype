Project News
============

You are welcome to follow GNOME on the following social networks:

* Twitter
* Fediverse (e.g. Mastodon, Pleroma)
* Facebook 
* Planet GNOME - an aggregator for blog posts from GNOME contributors 
* Discourse
* This Week in gnome
