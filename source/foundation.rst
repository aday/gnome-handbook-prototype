Foundation
==========

.. toctree::
   :hidden:

   foundation/board
   foundation/board-committees
   foundation/other-committees

The GNOME Foundation is the non-profit organization behind the GNOME Project. It is a membership-based organization, and every GNOME contributor is entitled to be a member, giving them a voice in how the Foundation is run. The Foundation provides leadership, financial backing, logistical support and legal services to the GNOME community. It also acts as a meeting point for the project's partners.

More information on the GNOME Foundation can be found on its web pages, including: governance, legal and trademarks, membership and contact details.

Parts of the Foundation
-----------------------

* Membership - every GNOME contributor is entitled to become a member
* Board of Directors - responsible for governing the Foundation, elected annually by the membership
* Advisory Board - fee-paying and non-fee-paying partners who regularly meet with the Board of Directors
* Staff - the Foundation employs a small number of people, including an Executive Director, Director of Operations, and contractors
* Committees - appointed by the board to do Foundation work (see /Committees for more details about their powers and responsibilities). These include Sponsorship, Membership and elections, Travel and Engagement 

Resources
---------

* GNOME Foundation Bylaws - the Foundation's constitution, which legally defines how it is governed
* GNOME Foundation Charter - the original definition of the Foundation, which states its goals and purpose
* GNOME Foundation Annual Reports
* List of previous GNOME Foundation referenda
* Hardware that is owned by the Foundation 
* More resources can be found on the Board of Directors resources page.

Policies
--------

* Board attendance policy
* Budget and spending policies
* Code of Conduct (including speaker guidelines)
* Conference requirements
* Conflict of interest policy
* Official software policy
* Privacy policy (work in progress
* Travel policy
