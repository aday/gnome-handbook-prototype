Email Alias Policy
------------------

Mail aliases (we do not host real mail spools due to limited resources) on the @gnome.org domain are a benefit reserved to GNOME Foundation members. Aliases are automatically created for every GNOME Foundation member. Updating the e-mail address the alias should relay to can be done by logging in with your account HERE.

Resetting your password

If you don't know your password when accessing the link above, you can have it reset via:

https://password.gnome.org
