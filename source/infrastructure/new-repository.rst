New Repositories
================

New repository request (own user namespace)

For these kind of requests, there's no need to own a GNOME (LDAP) account and it's sufficient to have logged in via GitLab by using any of the currently enabled third-party authentication providers.

Creating a new Git repository within your own user namespace is as straightforward as visiting https://gitlab.gnome.org/projects/new and entering the project name. Please make sure the namespace is set as your username (default). An example:

Project URL: https://gitlab.gnome.org/$username/$project_name

Please make sure your software adheres to the GNOME's Projects/Prerequisites as the GNOME GitLab instance is not a general purpose Git repository storage like gitlab.com, github.com are.

Inclusion in the World group

If your project is growing and you would like your project to be included in a more visible and decentralized GitLab group, you can create a request to add your project to the World group. To do so, file a ticket on GNOME's GitLab Infrastructure project with your ask and summary for the inclusion and we will review it.

New repository request (GNOME namespace)

1. Project Prerequisites

For these kind of requests, please make sure you own a GNOME (LDAP) account. Request it via NewAccounts.

There's currently an undergoing discussion on splitting the GNOME group into multiple little groups leaving /GNOME to only contain core modules. The plan will define a set of guidelines which are going to be enforced by a specific committee in charge of deciding where a specific module is expected to be hosted in GitLab (i.e GNOME Core, GNOME Circle groups etc.).

The re-org plan is being discussed at https://discourse.gnome.org/t/official-proposal-how-we-define-gnome-software/3371. Until the committee will be elected and the new model will go live new GNOME modules inclusions will be acknowledged by the GNOME Release Team.

2. Repository request

Once you have made sure your project is compliant to the prerequisites outlined before, please open an issue and make sure the following information is provided as part of the request:

    Repository name, which must be lowercase letters, digits, and '-'. (With an exception to allow uppercase for names starting with "perl-")

    Original repository source: external repositories hosting (GitHub, Bitbucket, GitLab.com etc.) or new repository in case the project has to be started from scratch 

3. Importing branches from your local repository

You can now push the branches and tags you want into your new repository. If you have a local repository with exactly the tags and branches you want (use git tag -l and git branch to list your tags and branches), then you can import all branches and tags with:

git remote add origin git@gitlab.gnome.org:GNOME/repository-name.git
git push origin --all

Check to make sure that you are happy with the contents of your repository (You can browse it on the web at https://gitlab.gnome.org/GNOME/<repository-name>.) 
