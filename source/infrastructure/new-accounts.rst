New Accounts
============

What technology is used for GNOME accounts?

GNOME accounts are LDAP accounts, created on our identity management (IdM) solution, which is based on FreeIPA.

When are GNOME accounts required?

GNOME accounts are currently required in three cases:

* Developers who need to directly commit code to modules in the core GNOME GitLab group
* Contributors who need to use services which are restricted to GNOME accounts owners. This includes Nextcloud, Openshift and meet.gnome.org.
* All GNOME Foundation members, who automatically get an account with their membership 

GNOME accounts are not required to contribute code changes to GNOME modules, as this can be done through merge requests in GitLab, and GitLab can be accessed using a third-party authentication provider (Google, GitHub, GitLab.com). Please read get involved for more information on how to get involved in a specific team.

How to request a GNOME (LDAP) account
-------------------------------------

Pre-requisites
~~~~~~~~~~~~~~

Account applicants must be endorsed by an existing GNOME contributor or module maintainer. Applicants must also demonstrate that they have been working with a specific GNOME team either writing code, documentation or providing other general purpose contributions before applying for a GNOME account.

Steps

* Login into gitlab.gnome.org using one of the available third-party authentication providers (Gitlab.com, GitHub, Google).
* Open an account request issue and only fill in the json dictionary composed of two (2) keys, action and module. The available actions are: git_access, master_access, update_email. The module value should only be specified with the git_access and maint_access actions and should point to the relevant GNOME module you are willing to request commit access to. For the update_email action please make sure the email you'd like to see synchronized on your GNOME Account is the primary one you registered on your GitLab account. The title of the account request issue should be "Account request". Don't worry about the issue being made confidential as automation will make sure that is taken care of for you.
* The GNOME Accounts Automation Bot will process your request by mailing relevant GNOME module maintainers, ask for their acknowledgement and create your account and associate requested rights against Gitlab's GNOME group or master.gnome.org.
* Once your application has been processed and the request issue has been closed, reset your GNOME Account password here. Your profile can then be managed by accessing https://idm01.gnome.org with your credentials.
* If you are planning on committing changes to GNOME modules, make sure that you read our documentation on using Git and being a maintainer.
* Once your GNOME (LDAP) account has been created, it will be linked to your existing GitLab account via a dedicated identity. For services that are not GitLab (i.e Nextcloud, Openshift, meet.gnome.org etc.) you should be able to login by entering the requested username and the password that was previously reset. 

Automation bot examples:

`{"action": "git_access", "module": "nautilus"}`

`{"action": "maint_access", "module": "gimp"}`

`{"action": "update_email"}`

For contributors who didn't have an account before and selected any of the git_access or maint_access actions:

* Your name, surname and username will be computed out of your full name as found in your GitLab profile
* The registered email on your GNOME account will be the one of your GitLab profile 

Other key takes when selecting the update_email action:

* The email that will be synchronized with your GNOME Account is the secondary one you have registered on your GitLab account, please make sure that one is up to date and doesn't match your @gnome.org alias or the e-mail address the alias points to 

Other key takes when selecting the maint_access action:

* This action will grant you access to master.gnome.org for committing new releases
* This action should only be requested by existing GNOME module maintainers, the app will make sure the DOAP file is updated before granting access 

Other account types
-------------------

blogs.gnome.org
~~~~~~~~~~~~~~~

See this page on how to create a blog on blogs.gnome.org.

@gnome.org email addresses
~~~~~~~~~~~~~~~~~~~~~~~~~~

@gnome.org mail aliases are currently created automatically within 24h from your acceptance as a member of the GNOME Foundation (the e-mail the alias will be binded to is the one available on your user profile page on the Account Management System).

master.gnome.org access
~~~~~~~~~~~~~~~~~~~~~~~

Accessing master.gnome.org is required for publishing new releases. It requires the DOAP file of the module that you will be publishing to contain a maintainer tag with your userid.

To apply, please open a new account request issue and specify the master_access action with the module you'd be willing to do releases for.

How to manage an existing GNOME (LDAP) account
----------------------------------------------

Users can log into idm01.gnome.org to update their personal information and SSH keys. If you have lost your account details, see the instructions on how to reset or retrieve your first time GNOME account password. 
