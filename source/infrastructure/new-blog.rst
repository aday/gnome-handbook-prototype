New Blogs
=========

Pre-requisites

You should have an email alias for gnome.org, gtk.org or gimp.org.

Sign-up

Sign up for your blog at the sign-up page. Choose something sensible for your blog name, such as your user name, IRC nick or project Git module name. There is only one namespace, and we expect people to play nice.

Make sure to use your @{gnome,gtk,gimp}.org alias when signing up or the form will refuse the blog addition.

Handling

None. You will receive an email with your blogs.gnome.org password, and a link to your brand new blog. There is currently no relationship between passwords on GNOME Blogs and other services.

Planet GNOME

To be added to Planet GNOME, please visit the Planet GNOME page for detailed information.

Please file a request in the issue tracker if you want your blog account to appear on planet.gnome.org. Generally, Foundation members and committers are added without question. If you're neither, but write a great blog and have made great contributions to GNOME, don't be afraid to ask. Then you should probably request Foundation membership!


