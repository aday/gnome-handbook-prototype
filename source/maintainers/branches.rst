Branches
========

The “master” branch (what you get when you do a simple git clone) is used for development of the unstable branch, with maintenance of the stable branch on a git branch. The stable branch is usually created just before the unstable branch is declared stable, because that is when some people start working on the next unstable development phase.

We try to use the same git branch names for all core GNOME modules. For instance, gnome-2-14 would be an excellent choice for a branch name targeting the GNOME 2.14 release, according to these rules:

    Use only lowercase characters in branch names.
    Use hyphens in branch names rather than underscores
    Use the format “gnome-MAJOR-MINOR” for the branch name if your branch is targeted for the GNOME MAJOR.MINOR release. Things such as the module name or the word “branch” are not necessary. 

Example

Here’s an example, when branching for 2.14 (inside a clone of the module!):

git branch gnome-2-14 master
git push origin gnome-2-14
git branch -d gnome-2-14
git checkout -b gnome-2-14 origin/gnome-2-14

where origin is the local name for the GNOME Git repository. The last two commands is to ensure that your local branch is properly setup to track the remote branch.

If you forgot branching and want to use an older commit for branching, for example to have the last 3 commits in master not included in your new gnome-2-14 branch, use “HEAD~x” (where x stands for the number of latest commits to exclude from your new branch) instead of “master”:

git branch gnome-2-14 HEAD~3
git push origin gnome-2-14

A shorter way to create a new remote branch & track it within a local branch is (using a fully qualified ref spec):

git push origin origin:refs/heads/gnome-2-14
git checkout -b gnome-2-14 origin/gnome-2-14

To delete a faulty remote branch use:

#potentially dangerous!
git push origin :gnome-2-14

If unsure use the --dry-run option for git-push first. 
