Making a release
================

Releases are how maintainers tell the world—or, more accurately, downstream developers—that it's time to update their dependencies.
