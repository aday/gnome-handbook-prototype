Release Planning
================

.. toctree::
   :hidden:

   release-planning/schedule
   release-planning/freezes
   release-planning/what-we-release


These pages are maintained by the release team and include information about the GNOME release process.

:doc:`Schedule for the current development cycle <release-planning/schedule>`.

GNOME versions
--------------

New version every six months, denoted by a whole integer: 40, 41, 42, etc. Updates to a major version number are included in point releases, such as 40.1, 40.2.

Each development cycle includes a number of unstable development releases: alpha, beta and release candidate (rc). Development releases are typically accompanied by a freeze policy - see :doc:`freezes <release-planning/freezes>`.

What we release
---------------

The modules that are built, tested and released as part of each GNOME version are defined in the gnome-build-meta module.

Background on why we include what we do is included in :doc:`what we release <release-planning/what-we-release>`.

