Travel
======

The Travel Committee is now accepting applications for travel funding via GitLab.

One of the ways that the GNOME Foundation supports the GNOME Project is by sponsoring contributors to attend GNOME events such as conferences and hackfests. Travel sponsorship requests are processed by the Travel Committee.

The GNOME Foundation must maintain effective control of expenses in order to maintain its financial viability and tax exempt status. The Foundation is also accountable to our donors to ensure that we manage their contributions wisely and maximize our ability to pursue our charitable mission. As such, the Foundation expects travelers to use good judgment and to claim reimbursement for only those expenses that are necessary and reasonable.

Read the guidelines below to find out if you qualify, and follow the instructions below to request sponsorship.

Don't panic, just ask. The bar required for travel sponsorships in general is not high. They are intended to maximize participation and contribution, not to make sponsorships a lofty prize only for the few. Don't let the size of this page fool you — it's an easy process. If you're asking yourself, "Should I apply?", the answer is "YES!"

NOTE: This sponsorship policy for GNOME Project contributors is different from the GNOME Foundation Travel and Reimbursable Expense Policy, although some sections are similar.
