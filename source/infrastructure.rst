Infrastructure
==============

GNOME infrastructure provides the platforms and tools used to create GNOME as well as other project activities.

Our primary platform is Gitlab, which provides issue tracking, code and content hosting, and more.

Other services:

* blogs.gnome.org
* Hegedoc
* meet.gnome.org (Big Blue Button)
* Nextcloud
* survey.gnome.org

To check the status of GNOME infrastructure, see status.gnome.org.

Accounts
--------

To use GNOME infrastructure, you typically need an account. The following pages include instructions on requesting new accounts, blogs and code repositories.

Other infrastructure pages
--------------------------

.. toctree::

   infrastructure/new-accounts
   infrastructure/new-blog
   infrastructure/new-repository
   infrastructure/ssh-key-guidelines
   infrastructure/email-alias-policy
