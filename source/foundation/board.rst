Foundation Board
================

The Board of Directors governs the GNOME Foundation, the non-profit organization behind the GNOME Project. For general information, see the Foundation wiki page and web pages.

Board Members
-------------

* Current board members
* Previous board members
* Previous boards by year 

Roles and Responsibilities
--------------------------

President
~~~~~~~~~

* Acts as the chair if no chair is assigned
* Acts as the Executive Director, if there isn't one
* Has contract signing and spending authorization powers
* Is the link between the board and the Executive Director, and acts as the ED's line manager
* Sits on the compensation committee
* Sits on the executive committee
* Sits on the finance committee

This post is currently held by Robert McQueen 

Vice-Presidents
~~~~~~~~~~~~~~~

Stands in for the President in their absence

This post is currently held by Regina Nkemchor Adejo and Alberto Ruiz 

Chair
~~~~~

* This is an optional position
* Prepares an agenda and sends it to the board before each meeting
* Chairs each board meeting
* Sits on the governance committee

This post is currently held by Thibault Martin 

Treasurer
~~~~~~~~~

* Ensures board oversight of the Foundation's financials
* Regularly reports to the board on the Foundation's finances (cash, receivables, outstanding bills, expenditures, income)
* Sits on the compensation committee
* Sits on the finance committee

This post is currently held by Sammy Fung 

Assistant Treasurer
~~~~~~~~~~~~~~~~~~~

Assists the treasurer

This post is held by Rosanna Yuen 

Secretary
~~~~~~~~~

* Takes minutes of board and advisory board meetings
* Publishes meeting minutes
* Takes note of actions and discussion since the preceding meeting (such as items discussed on the board mailing list) and ensures they are included in the meeting minutes.
* Strives to keep information (such as process documentation) on the GNOME Foundation website and wiki organized.

This post is currently held by Philip Chimento. 

Vice-Secretary
~~~~~~~~~~~~~~

Acts as the secretary if they are absent, and generally helps with secretarial duties

This post is currently held by Martin Abente Lahaye. 

General Responsibilities
------------------------

In addition to the above roles, all board members are required to:

* Attend board meetings (currently a two-hour meeting every every month) - see board attendance policy.
* Attend the two days of in-person meetings prior to GUADEC.
* Promptly respond to board-list, particularly votes that are held there.
* Occasionally take on board tasks. These are typically small and often require you to liase with other individuals or organizations. However, sometimes they are larger in scope.
* Don't block initiatives: don't volunteer to do tasks that you're incapable of doing. If you don't think you have time to complete a task, let the rest of the board know.
* Represent the board when interacting with community members. If someone raises concerns or issues in your presence, respond appropriately. Consider whether it's an issue that needs to be brought to the rest of the board. Remember that you're an elected representative.
* Keep confidential discussions private. This includes legal discussions or conversations with the Advisory Board. 

Meetings
--------

The board has its regular meeting on the second Monday of every month.

Public minutes of board meetings are sent to foundation-announce, foundation-list, and archived on the wiki. See the following section to see what is minuted.

General information on meeting minutes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Board publishes its meeting minutes in for archival at https://wiki.gnome.org/FoundationBoard/Minutes. The minutes are also sent to the foundation-announce@gnome.org and foundation-list@gnome.org mailing lists.

The secretary of the board (understood to include either the secretary, vice-secretary, or their deputy) is responsible for recording the minutes during board meetings.

During each meeting, it is customary for the Board to review and finalize the minutes of its last meeting, and to publish them as soon as possible. Given the frequency of Foundation Board meetings (once every two weeks), Foundation members should expect to see published minutes no later than two weeks after the meeting to which they correspond.

Please see the Guidelines on meeting minutes, approval, and confidentiality for more information.

Confidential topics
~~~~~~~~~~~~~~~~~~~

In many nonprofit organizations, it is common practice for the minutes of the board meetings to be confidential. Given the mission of the GNOME Foundation, the board should aspire to more openness than this common practice, and historically the GNOME Foundation has published its board meeting minutes publicly by default, except for topics where the board saw a need for confidentiality.

Sometimes a topic discussed in the board meeting should not be included in the published minutes. This is at the discretion of the board, and can be for reasons of individual or organizational privacy, or it can be due to a need to speak candidly or critically of a person, situation, or organization. Example confidential topics include, but are not limited to, the following:

* Discussions of personal conflicts between Foundation members.
* Discussions of the personal finances of Foundation members, such as individual travel sponsorship applications.
* Discussions about the hiring, compensation, or job performance of Foundation staff.
* Discussions of relationships between the GNOME Foundation and other organizations.
* Discussions about recruiting individual Foundation members to volunteer positions.
* Discussions of ongoing legal proceedings involving the GNOME Foundation or its members or staff. 

Finally, another guiding principle is that when someone speaks with the expectation that they are speaking in confidence, a record of the conversation must not be published retroactively in the future.

If applicable, published meeting minutes may contain a summary of confidential topics, that omits any confidential details.

There are no “temporarily confidential” topics. It happens often that a topic contains no confidential information, but should not be published until some action item is completed: e.g. an announcement is made, or correspondence is answered. In that case, if the action is not completed when it is time to publish the minutes, the foundation-announce list should be notified.

Who to contact when
-------------------

The GNOME Foundation has staff and committees that handle much of the GNOME Foundation's business. Most inquiries about GNOME Foundation business should be directed to info@gnome.org.

For questions about licensing the GNOME Foundation's trademarks, please contact licensing@gnome.org.

The board of directors can be contacted at board-list@gnome.org. They should be contacted for inquiries about committees, policies, and board meetings. The board is also the last point of escalation in case you have an issue that you feel has not been satisfactorily handled elsewhere in the GNOME Foundation.

More tips
---------

* Ways to help the board help you 

Resources
---------

* Results from GNOME Foundation elections and referenda
* GNOME 501(c) non-profit Determination Letter 

More resources, including legal resources and documents, can be found on the resources page.
