Module Maintenance
==================

Module maintainers maintain modules. These pages contain information.

Guidelines for new maintainers
------------------------------

Here are some things you should do when you’re becoming maintainer for a module:

* first, ask the previous maintainer(s) or the co-maintainer(s) what you should do :-)
* add yourself to the [module].doap file
* login to Discourse and subscribe to the announcement tag as well as your module specific tag
* subscribe to devel-announce-list for release-team and other developer-related announcements. Note: Discourse is preferred over mailing lists
* be sure to subscribe to your project(s) in Gitlab to receive notifications
* subscribe to the wiki page of your module, if it exists
* read/update the triage guidelines for your module
* read the various documentations available in the link section, particularly the GNOME module requirements 

Other information
-----------------

.. toctree::

   maintainers/making-a-release
   maintainers/branches
