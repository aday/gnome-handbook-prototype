GNOME Project Handbook
======================

Welcome to the GNOME project!

This handbook contains all the information you need to participate in GNOME. It includes links to our websites and development platforms, information about how the project is managed, as well as guides for getting setup as a contributor.

If you spot missing or inaccurate information, please file an issue or a merge request.

.. toctree::
   :hidden:

   get-in-touch
   news
   teams
   governance
   contribute
   infrastructure
   release-planning
   tools
   docs
   maintainers
   events
   foundation
   coc
