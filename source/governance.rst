Governance
==========

The GNOME Project is an open community of contributors. Anyone can contribute to GNOME under the terms of the license for a particular component.

Technical and design decisions are made informally between the relevant contributors, and the project as a whole tries to avoid unnecessary bureaucracy. However, we do work to engage the relevant teams and contributors in decisions, in order to coordinate development across the project, and to draw on non-coding expertise.

Maintainers and Release Team
----------------------------

Each GNOME component (or "module") has one or more maintainers, and they generally have the final say over which changes are included in their components.

The Release Team has the final say over which components (and versions of components) go into each GNOME release, as well as which components are classified as GNOME software. While the Release Team does not routinely weigh in on individual technical decisions, it is the closest thing that GNOME has to a technical governing body.

The Release Team is a committee of the GNOME Foundation Board of Directors. Its power to define GNOME software has been delegated to it by the board.

GNOME Foundation
----------------

The GNOME Foundation is the non-profit organization behind the GNOME Project. It has a staff and its operations are managed by the Executive Director, who has the authority to make decisions on behalf of the GNOME Foundation.

The Foundation takes care of essential aspects of the GNOME project, including its technical infrastructure and events. It is also the main fundraising body for GNOME, and carries out programs which further its wider mission.

Ultimate control over what software is defined as GNOME rests with the GNOME Foundation, as the owner of the GNOME trademarks.

The Foundation is governed by the GNOME Foundation Board of Directors. The board sets the overall direction for the Foundation, and is elected by the GNOME Foundation Membership, which GNOME contributors are eligible to join.

See the Foundation website and wiki pages for more information. 
